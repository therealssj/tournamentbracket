<?php
namespace App\Traits\Users;

trait HasTeams
{
    public function teams()
    {
        return $this->belongsToMany('App\Models\Team', 'team_user');
    }
}