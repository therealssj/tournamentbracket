<?php

namespace App\Traits\Teams;

use App\Models\User;

trait TeamTrait
{

    // Removes a user from a team.
    public function removeUser(User $user)
    {
        return $this->members()->toggle($user);
    }

    // Check whether a user is in a team.
    public function hasMember(User $user)
    {
        return $this->members()->where('id', "=", $user->id)->first() ? true : false;
    }

    // members of a team.
    public function members()
    {
        return $this->belongsToMany('App\Models\User', 'team_user');
    }

}