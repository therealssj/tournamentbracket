<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;

class UsersController extends Controller
{
    /**
     * The user model.
     *
     * @var \App\Models\User
     */
    protected $userModel;

    public function __construct(User $userModel)
    {
        $this->userModel = $userModel;
    }

    /**
     * Return all users int the database.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $users = $this->userModel->all();
        return response()->json($users->toArray());
    }

    /**
     * Fetch user from id.
     *
     * @param $id (int)
     *  The user id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $user = $this->userModel->find($id);
        if (!$user) {
            return $this->recordNotFound();
        }
        return response()->json($user);
    }

    /**
     * Delete user using id.
     *
     * @param $id (int)
     *  The user id.
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->userModel->find($id);
        if (!$user) {
            return $this->recordNotFound();
        }
        $user->delete();
        return response(null, 200);
    }
}
