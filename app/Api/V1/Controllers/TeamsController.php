<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Team;
use Illuminate\Http\Request;

class TeamsController extends Controller
{
    /**
     * The team model.
     *
     * @param \App\Models\Team $teamModel
     */
    protected $teamModel;

    public function __construct(Team $teamModel)
    {
        $this->teamModel = $teamModel;
    }

    /**
     * Return all the existing teams in the database.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $teams = $this->teamModel->all();
        return response()->json($teams->toArray());
    }

    /**
     * Fetch team information from the id.
     *
     * @param $id (int)
     *  The team id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $team = $this->teamModel->find($id);
        if (!$team) {
            return $this->recordNotFound();
        }
        return response()->json($team);
    }

    /**
     * Store a team's informtion.
     *
     * @param \Illuminate\Http\Request $request
     *  Request resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name' => 'required|max:255',
          'description' => 'required'
        ]);
        $team = new $this->teamModel([
          'name' => $request->input('name'),
          'slug' => str_slug($request->input('name')),
          'description' => $request->input('description'),
          'team_information' => 'le default value',
        ]);
        $team->save();
        return response()->json($team);
    }

    /**
     * Update team information.
     *
     * @param \Illuminate\Http\Request $request
     *  Request resource.
     * @param $id (int)
     *  The team id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name' => 'required|max:255',
          'description' => 'required'
        ]);
        $team = $this->teamModel->find($id);
        if (!$team) {
            return $this->recordNotFound();
        }
        $team->name = $request->input('name');
        $team->slug = str_slug($request->input('name'));
        $team->description = $request->input('description');
        $team->save();
        return response()->json($team);
    }

    /**
     * Delete user using id.
     *
     * @param $id (int)
     *  The team id.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = $this->teamModel->find($id);
        if (!$team) {
            return $this->recordNotFound();
        }
        $team->delete();
        return response(null, 200);
    }
}
