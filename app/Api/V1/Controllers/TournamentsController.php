<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Team;
use App\Models\Tournament;
use Dingo\Api\Http\Request;

class TournamentsController extends Controller
{
    /**
     * Tournament model.
     *
     * @var \App\Models\Tournament
     */
    protected $tournamentModel;

    /**
     * Team model.
     *
     * @var \App\Models\Team
     */
    protected $teamModel;

    /**
     * The game state.
     */
    private $metadata;

    public function __construct(Tournament $tournamentModel, Team $teamModel)
    {
        $this->tournamentModel = $tournamentModel;
        $this->teamModel = $teamModel;
    }

    /**
     * Return all the existing tournaments in the database.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $tournaments = $this->tournamentModel->all();
        return response()->json($tournaments->toArray());
    }

    /**
     * The tournament id.
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $tournament = $this->tournamentModel->find($id);
        if (!$tournament) {
            return $this->recordNotFound();
        }

        return response()->json($tournament);
    }

    /**
     * @param \Dingo\Api\Http\Request $request
     *
     * @return array
     */
    public function play(Request $request)
    {
        // @todo Add proper validation rules.
        //$this->validate($request, [
        //
        //]);
        $tournamentId = $request->get('id');
        if( !$this->metadata ) {
            $tournament = $this->tournamentModel->find($tournamentId);
            $teams = explode(',', $request->get('teams'));
            $teamCount = count($teams);

            $bracketSize = $tournament->size;
            $metadata = $request->get('metadata');
            $playersPerTeam = $this->fetchPlayersPerTeam($teamCount, $bracketSize);
            if ( !$playersPerTeam ){
                return response(null, 400);
            }
            else{
               $this->metadata = $this->buildMetaData($tournamentId, $teams, $playersPerTeam, $bracketSize);
            }
        }else{
            if ($this->metadata['tournamentId'] != $request->get('id')) {
                // is the player trying to cheat?
                // maybe drop the current session.
                return response(null, 400);
            }
            $winners = explode(',', $request->get('winners'));
            $wCnt = count($winners);
            if ($wCnt == 1)
                return response(null, 400);
            $rivalTemp = [];
            for($i=0,$j=0; $i<($wCnt/2); $i++,$j++){
                $rivalTemp[$i] = [$winners[$j], $winners[$j++]];
            }

            $this->metadata['rivals'] = $rivalTemp;
        }
        
        return $this->metadata;
    }

    /**
     * No. of players to fetch from one team.
     *
     * @param $teamSize
     *  Number of teams playing.
     * @param $bracketSize
     *  The tournament bracket size.
     *
     * @return bool|int
     */
    protected function fetchPlayersPerTeam($teamSize, $bracketSize){
        if( ($temp = (int)($bracketSize/$teamSize))  == 0 ){
            return false;
        }

        return $temp*2;
    }

    /**
     * Build initial metadata for the tournament.
     *
     * @param $tournamentId
     *  The id of the current tournament.
     * @param $teams
     *  Comma separated team ids.
     * @param $playersPerTeam
     *  Max number of players to take from a team.
     * @param $bracketSize
     *  The tournament bracket size.
     *
     * @return array
     *  Initial metadata.
     */
    protected function buildMetaData($tournamentId, $teams, $playersPerTeam, $bracketSize){
        $tempMeta = [];
        $players = [];
        $rivals = [];
        $max = 0;
        foreach($teams as $teamId){
            $teamMembers = $this->teamModel->find($teamId)->members;
            $players[$teamId] = [];
            if ($teamMembers->count() > $playersPerTeam) {
                $temp = [];
                while(count($temp) < $playersPerTeam) {
                    $temp[mt_rand(0, $teamMembers->count()-1)] = 1;
                }
                if ( ($x = count($temp)) > $max )
                    $max = $x;
                foreach($temp as $key => $val){
                    $players[$teamId][] = $teamMembers[$key];
                }
            }else{
                if ( ($x = count($teamMembers)) > $max )
                    $max = $x;
                $players[$teamId] = $teamMembers;
            }
        }

        for($i=0; $i<$max; $i++){
            if (count($players) == 1){
                foreach($players as $id => $playerList){
                    foreach($playerList as $player){
                        $rivals[] = [$id => $player, ''];
                    }
                }
                break;
            } else {
                $randTeamIds = array_rand($players, 2);
                $rivals[$i] = [
                  $randTeamIds[0] => array_pop($players[$randTeamIds[0]]),
                  $randTeamIds[1] => array_pop($players[$randTeamIds[1]])
                ];

                array_filter($players);
            }
        }

        //if( ($t = count($rivals)) < $bracketSize) {
        //    while( $t < $bracketSize ) {
        //        $rivals[] = ['',''];
        //    }
        //}

        $tempMeta['tournamentId'] = $tournamentId;
        $tempMeta['currentBracket'] = $bracketSize;
        $tempMeta['rivals'] = $rivals;

        return $tempMeta;
    }

    // Upload score or image of score.
    public function upload(Request $request)
    {
        // @todo TBD.
    }
}
