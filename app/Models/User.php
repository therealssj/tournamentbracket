<?php
namespace App\Models;

use App\Models\Team;
use App\Traits\Users\HasTeams;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasTeams;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'email', 'password', 'remember_token',
    ];

    /**
     * The attributes that should be mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'username', 'name', 'password'
    ];
}