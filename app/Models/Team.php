<?php

namespace App\Models;

use App\Traits\Teams\TeamTrait;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use TeamTrait;
    /**
     * The attributes that should be mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name',
      'slug',
      'description',
      'team_information',
    ];

    // A team can have many members.
    public function members()
    {
        return $this->belongsToMany('App\Models\User', 'team_user');
    }
}
