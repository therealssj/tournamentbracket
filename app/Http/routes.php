<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'tournaments'], function (Router $api) {
        $api->get('info', 'App\\Api\\V1\\Controllers\\TournamentsController@index');
        $api->get('info/{id}', 'App\\Api\\V1\\Controllers\\TournamentsController@show');

        $api->post('play', 'App\\Api\\V1\\Controllers\\TournamentsController@play');

        $api->post('upload', 'App\\Api\\V1\\Controllers\\TournamentsController@upload');
    });
});
